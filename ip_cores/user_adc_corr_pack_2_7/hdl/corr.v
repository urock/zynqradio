`timescale 1ns / 1ps

`define CLOG2(x) 	  \
   (x <= 2)    ? 1  : \
   (x <= 4)    ? 2  : \
   (x <= 8)    ? 3  : \
   (x <= 16)   ? 4  : \
   (x <= 32)   ? 5  : \
   (x <= 64)   ? 6  : \
   (x <= 128)  ? 7  : \
   (x <= 256)  ? 8  : \
   (x <= 512)  ? 9  : \
   (x <= 1024) ? 10 : \
   -1
	
	module corr # (
		parameter CORR_SIZE 	   = 128,
//		parameter SUM_LATENCY	   = 1,
		parameter INPUT_DATA_WIDTH = 16
	)
	(
		input  wire                         clk, 
		input  wire                         clk_ena,
		input  wire [INPUT_DATA_WIDTH-1:0]  data,
		input  wire [CORR_SIZE-1:0]         mseq,
		input  wire                         rst,
		output wire [2*INPUT_DATA_WIDTH-1:0] sum
	);

	localparam SUM_DEPTH = (`CLOG2(CORR_SIZE))+1;
//	localparam SUM_STEP = SUM_DEPTH/(SUM_LATENCY+1);
	
	reg signed [(INPUT_DATA_WIDTH-1):0] shift_reg [0:CORR_SIZE-1];
	reg signed [(INPUT_DATA_WIDTH-1):0] mult_reg  [0:CORR_SIZE-1];
	reg signed [(2*INPUT_DATA_WIDTH-1):0] tmp [0:CORR_SIZE-1][0:SUM_DEPTH-1];
	integer i;
	
			
	always@(posedge clk or posedge rst) begin
		if (rst) begin
			for (i=(CORR_SIZE-1);i>=0;i=i-1) shift_reg[i] <= 0;
		end
		else if (clk_ena) begin
			for (i=(CORR_SIZE-1);i>0;i=i-1) shift_reg[i] <= shift_reg[i-1];
			shift_reg[0] <= data;
		end
	end
	
	always @* begin
		for (i=(CORR_SIZE-1);i>=0;i=i-1) begin
			mult_reg[i] <= (shift_reg[i]^{INPUT_DATA_WIDTH{mseq[CORR_SIZE-1-i]}})+mseq[CORR_SIZE-1-i];
		end
	end
	
	
	//always @* for (i=(CORR_SIZE-1);i>=0;i=i-1) tmp[i][0] = mult_reg[i];
	//generate
	//	genvar m, n;
	//	for (n=1; n < SUM_DEPTH; n=n+1) begin
	//		localparam p = n+1;
	//		if ((p==SUM_STEP || p==2*SUM_STEP || p==3*SUM_STEP || p==4*SUM_STEP || p==5*SUM_STEP || p==6*SUM_STEP || p==7*SUM_STEP || p==8*SUM_STEP) && p<=(SUM_DEPTH-SUM_STEP))
	//			for(m=0; m < (CORR_SIZE / (1 << n)); m=m+1) begin
	//				always @(posedge clk  or posedge rst) begin
	//					if (rst) begin
	//						tmp[m][n] <= 0;
	//					end else if (clk_ena) begin 
	//						tmp[m][n] = tmp[2*m][n-1] + tmp[2*m+1][n-1];
	//					end
	//				end
	//			end
	//		else for(m=0; m < (CORR_SIZE / (1 << n)); m=m+1) begin
	//			always @* tmp[m][n] = tmp[2*m][n-1] + tmp[2*m+1][n-1];
	//		end
	//	end
	//endgenerate
	
	always @(posedge clk  or posedge rst) begin
		if (rst) begin
			for (i=(CORR_SIZE-1);i>=0;i=i-1) tmp[i][0] <= 0;
		end else if (clk_ena) begin 
			for (i=(CORR_SIZE-1);i>=0;i=i-1) tmp[i][0] <= mult_reg[i];
		end
	end
	generate
		genvar m, n;
		for (n=1; n < SUM_DEPTH; n=n+1) begin
			for(m=0; m < (CORR_SIZE / (1 << n)); m=m+1) begin
				always @(posedge clk  or posedge rst) begin
					if (rst) begin
						tmp[m][n] <= 0;
					end else if (clk_ena) begin 
						tmp[m][n] = tmp[2*m][n-1] + tmp[2*m+1][n-1];
					end
				end
			end
		end
	endgenerate
	
	assign sum = tmp[0][SUM_DEPTH-1];
	
	endmodule
