
`timescale 1 ns / 1 ps

	module adc_corr_pack #
	(
		// Users to add parameters here
		parameter CHANNELS = 4,
        parameter DATA_WIDTH = 16,
//        parameter SUM_LATENCY = 0,
        parameter CORR_SIZE = 128,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S_AXI
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		parameter integer C_S_AXI_ADDR_WIDTH	= 5
	)
	(
		// Users to add ports here
        input wire                  clk,
        input wire                   chan_enable_0,
        input wire                   chan_valid_0,
        input wire [DATA_WIDTH-1:0] chan_data_0,
        input wire                   chan_enable_1,
        input wire                   chan_valid_1,
        input wire [DATA_WIDTH-1:0] chan_data_1,
        input wire                   chan_enable_2,
        input wire                   chan_valid_2,
        input wire [DATA_WIDTH-1:0] chan_data_2,
        input wire                   chan_enable_3,
        input wire                   chan_valid_3,
        input wire [DATA_WIDTH-1:0] chan_data_3,
        input wire                   chan_enable_4,
        input wire                   chan_valid_4,
        input wire [DATA_WIDTH-1:0] chan_data_4,
        input wire                   chan_enable_5,
        input wire                   chan_valid_5,
        input wire [DATA_WIDTH-1:0] chan_data_5,
        input wire                   chan_enable_6,
        input wire                   chan_valid_6,
        input wire [DATA_WIDTH-1:0] chan_data_6,
        input wire                   chan_enable_7,
        input wire                   chan_valid_7,
        input wire [DATA_WIDTH-1:0] chan_data_7,
        output wire  [CHANNELS*DATA_WIDTH-1:0] ddata,
        output wire                           dvalid,
        output wire                            dsync,

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S_AXI
		input wire  s_axi_aclk,
		input wire  s_axi_aresetn,
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] s_axi_awaddr,
		input wire [2 : 0] s_axi_awprot,
		input wire  s_axi_awvalid,
		output wire  s_axi_awready,
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] s_axi_wdata,
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] s_axi_wstrb,
		input wire  s_axi_wvalid,
		output wire  s_axi_wready,
		output wire [1 : 0] s_axi_bresp,
		output wire  s_axi_bvalid,
		input wire  s_axi_bready,
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] s_axi_araddr,
		input wire [2 : 0] s_axi_arprot,
		input wire  s_axi_arvalid,
		output wire  s_axi_arready,
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] s_axi_rdata,
		output wire [1 : 0] s_axi_rresp,
		output wire  s_axi_rvalid,
		input wire  s_axi_rready
	);
	
	wire [C_S_AXI_DATA_WIDTH-1:0]	 mseq0;
    wire [C_S_AXI_DATA_WIDTH-1:0]    mseq1;
    wire [C_S_AXI_DATA_WIDTH-1:0]    mseq2;
    wire [C_S_AXI_DATA_WIDTH-1:0]    mseq3;
    wire 						     mpush;    
    wire 						     corr_rst;
    wire [C_S_AXI_DATA_WIDTH-1:0]	 data_sel;
    wire                             adc_chan_valid;
    wire [DATA_WIDTH-1:0]            adc_pack_data_0;
    wire [DATA_WIDTH-1:0]            adc_pack_data_1;
    wire [DATA_WIDTH-1:0]            adc_pack_data_2;
    wire [DATA_WIDTH-1:0]            adc_pack_data_3;
    
    
// Instantiation of Axi Bus Interface S_AXI
	adc_corr_pack_S_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH)
	) adc_corr_pack_S_AXI_inst (
	    .mseq0(mseq0),
	    .mseq1(mseq1),
        .mseq2(mseq2),
        .mseq3(mseq3),
        .mpush(mpush),
        .corr_rst(corr_rst),
        .data_sel(data_sel),
		.S_AXI_ACLK(s_axi_aclk),
		.S_AXI_ARESETN(s_axi_aresetn),
		.S_AXI_AWADDR(s_axi_awaddr),
		.S_AXI_AWPROT(s_axi_awprot),
		.S_AXI_AWVALID(s_axi_awvalid),
		.S_AXI_AWREADY(s_axi_awready),
		.S_AXI_WDATA(s_axi_wdata),
		.S_AXI_WSTRB(s_axi_wstrb),
		.S_AXI_WVALID(s_axi_wvalid),
		.S_AXI_WREADY(s_axi_wready),
		.S_AXI_BRESP(s_axi_bresp),
		.S_AXI_BVALID(s_axi_bvalid),
		.S_AXI_BREADY(s_axi_bready),
		.S_AXI_ARADDR(s_axi_araddr),
		.S_AXI_ARPROT(s_axi_arprot),
		.S_AXI_ARVALID(s_axi_arvalid),
		.S_AXI_ARREADY(s_axi_arready),
		.S_AXI_RDATA(s_axi_rdata),
		.S_AXI_RRESP(s_axi_rresp),
		.S_AXI_RVALID(s_axi_rvalid),
		.S_AXI_RREADY(s_axi_rready)
	);
        
	// Add user logic here	  
    locator_data_sel # (
        .DATA_WIDTH(DATA_WIDTH),
        .C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
//        .SUM_LATENCY(SUM_LATENCY),
        .CORR_SIZE(CORR_SIZE)
    ) locator_data_sel_inst (
        .clk(clk),
        .chan_enable_0(chan_enable_0),
        .chan_valid_0(chan_valid_0),
        .chan_data_0(chan_data_0),
        .adc_pack_data_0(adc_pack_data_0),
        .adc_chan_enable_0(adc_chan_enable_0),
        .chan_enable_1(chan_enable_1), 
        .chan_valid_1(chan_valid_1),    
        .chan_data_1(chan_data_1),
        .adc_pack_data_1(adc_pack_data_1),
        .adc_chan_enable_1(adc_chan_enable_1),
        .chan_enable_2(chan_enable_2),
        .chan_valid_2(chan_valid_2),
        .chan_data_2(chan_data_2),
        .adc_pack_data_2(adc_pack_data_2),
        .adc_chan_enable_2(adc_chan_enable_2),
        .chan_enable_3(chan_enable_3),
        .chan_valid_3(chan_valid_3),
        .chan_data_3(chan_data_3),
        .adc_pack_data_3(adc_pack_data_3),
        .adc_chan_enable_3(adc_chan_enable_3),
        .adc_chan_valid(adc_chan_valid),  
        .mseq0(mseq0),
        .mseq1(mseq1),
        .mseq2(mseq2),
        .mseq3(mseq3),
        .mpush(mpush),
        .corr_rst(corr_rst),
        .data_sel(data_sel)
    );
	
    util_adc_pack # (
        .CHANNELS(CHANNELS),
        .DATA_WIDTH(DATA_WIDTH)
    ) util_adc_pack_inst (
        .clk(clk),
        .chan_valid(adc_chan_valid|chan_valid_4|chan_valid_5|chan_valid_6|chan_valid_7),
        .chan_enable_0(adc_chan_enable_0),
        .chan_data_0(adc_pack_data_0),   
        .chan_enable_1(adc_chan_enable_1),
        .chan_data_1(adc_pack_data_1),   
        .chan_enable_2(adc_chan_enable_2),
        .chan_data_2(adc_pack_data_2),
        .chan_enable_3(adc_chan_enable_3),
        .chan_data_3(adc_pack_data_3),
        .chan_enable_4(chan_enable_4),
        .chan_data_4(chan_data_4),
        .chan_enable_5(chan_enable_5),
        .chan_data_5(chan_data_5),   
        .chan_enable_6(chan_enable_6),
        .chan_data_6(chan_data_6),
        .chan_enable_7(chan_enable_7),
        .chan_data_7(chan_data_7),    
        .ddata(ddata),
        .dvalid(dvalid),
        .dsync(dsync)
   );
    
	// User logic ends

	endmodule
