`timescale 1ns / 1ps

`define CLOG2(x) 	  \
   (x <= 2)    ? 1  : \
   (x <= 4)    ? 2  : \
   (x <= 8)    ? 3  : \
   (x <= 16)   ? 4  : \
   (x <= 32)   ? 5  : \
   (x <= 64)   ? 6  : \
   (x <= 128)  ? 7  : \
   (x <= 256)  ? 8  : \
   (x <= 512)  ? 9  : \
   (x <= 1024) ? 10 : \
   -1

    module locator_data_sel # (
        parameter DATA_WIDTH                    = 16,
        parameter C_S_AXI_DATA_WIDTH            = 32,
//        parameter SUM_LATENCY                   = 1,
        parameter CORR_SIZE                     = 128
    )
    (
        input wire                              clk,
        
        input wire                              chan_enable_0,
        input wire                              chan_valid_0,
        input wire  [DATA_WIDTH-1:0]            chan_data_0,
        output wire [DATA_WIDTH-1:0]            adc_pack_data_0,
        output wire                             adc_chan_enable_0,
        
        input wire                              chan_enable_1,
        input wire                              chan_valid_1,
        input wire  [DATA_WIDTH-1:0]            chan_data_1,
        output wire [DATA_WIDTH-1:0]            adc_pack_data_1,
        output wire                             adc_chan_enable_1,

        input wire                              chan_enable_2,
        input wire                              chan_valid_2,   
        input wire  [DATA_WIDTH-1:0]            chan_data_2,
        output wire [DATA_WIDTH-1:0]            adc_pack_data_2,
        output wire                             adc_chan_enable_2,
        
        input wire                              chan_enable_3,
        input wire                              chan_valid_3,
        input wire  [DATA_WIDTH-1:0]            chan_data_3,
        output wire [DATA_WIDTH-1:0]            adc_pack_data_3,
        output wire                             adc_chan_enable_3,
        
        output wire                             adc_chan_valid,
        
        input wire  [C_S_AXI_DATA_WIDTH-1:0]	mseq0,
        input wire  [C_S_AXI_DATA_WIDTH-1:0]    mseq1,
        input wire  [C_S_AXI_DATA_WIDTH-1:0]    mseq2,
        input wire  [C_S_AXI_DATA_WIDTH-1:0]    mseq3,
        input wire  						    mpush,
        input wire  						    corr_rst,
        input wire  [C_S_AXI_DATA_WIDTH-1:0]    data_sel
    );
    
    localparam CORR_LATENCY = (`CLOG2(CORR_SIZE))+2;
    
    wire [2*DATA_WIDTH-1:0] corr_data_I;
    wire [2*DATA_WIDTH-1:0] corr_data_Q;
    wire chan_valid;
    reg  [CORR_LATENCY-1:0] chan_valid_in_d;
    reg  [4*C_S_AXI_DATA_WIDTH-1:0]    mseq;
    integer index;
    
    corr #(
        .CORR_SIZE(CORR_SIZE),
//        .SUM_LATENCY(SUM_LATENCY),
        .INPUT_DATA_WIDTH(DATA_WIDTH)
    ) corr_I_inst (
        .clk(clk), 
        .clk_ena(chan_valid),
        .data(chan_data_0),
        .mseq(mseq),
        .rst(corr_rst),
        .sum(corr_data_I)
    );
    
    corr #(
        .CORR_SIZE(CORR_SIZE),
//        .SUM_LATENCY(SUM_LATENCY),
        .INPUT_DATA_WIDTH(DATA_WIDTH)
    ) corr_Q_inst (
        .clk(clk), 
        .clk_ena(chan_valid),
        .data(chan_data_1),
        .mseq(mseq),
        .rst(corr_rst),
        .sum(corr_data_Q)
    );
    
    assign chan_valid   = chan_valid_0 | chan_valid_1 | chan_valid_2 | chan_valid_3;
    assign adc_chan_valid  = (data_sel==1) ? chan_valid_in_d[0] : chan_valid;
    
    assign adc_chan_enable_0 = data_sel ? 1'b1 : chan_enable_0;
    assign adc_chan_enable_1 = data_sel ? 1'b1 : chan_enable_1;
    assign adc_chan_enable_2 = data_sel ? 1'b1 : chan_enable_2;
    assign adc_chan_enable_3 = data_sel ? 1'b1 : chan_enable_3;
    
    assign adc_pack_data_0 = (data_sel==2) ? chan_data_0 : 		(data_sel==1) ? corr_data_I[(DATA_WIDTH-1):0]			 : chan_data_0;
    assign adc_pack_data_1 = (data_sel==2) ? chan_data_1 : 		(data_sel==1) ? corr_data_I[(2*DATA_WIDTH-1):DATA_WIDTH] : chan_data_1;
    assign adc_pack_data_2 = (data_sel==2) ? chan_data_0 : 		(data_sel==1) ? corr_data_Q[(DATA_WIDTH-1):0]			 : chan_data_2;
    assign adc_pack_data_3 = (data_sel==2) ? chan_data_1 : 		(data_sel==1) ? corr_data_Q[(2*DATA_WIDTH-1):DATA_WIDTH] : chan_data_3;
    
    always @(posedge clk) begin
        chan_valid_in_d[CORR_LATENCY-1] <= chan_valid;
        for (index=(CORR_LATENCY-2); index>=0; index=index-1) begin
            chan_valid_in_d[index] <= chan_valid_in_d[index+1];
        end
    end
    
    always @(posedge clk) begin
        if (mpush) begin
            mseq <= {mseq3, mseq2, mseq1, mseq0};
        end
    end
    
endmodule
