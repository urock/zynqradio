/*
 * adc_corr_pack.h
 *
 *  Created on: 25.02.2015
 *      Author: ����
 */

#ifndef ADC_CORR_PACK_H_
#define ADC_CORR_PACK_H_

/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "ad9361.h"
#include "dac_core.h"
#include "adc_core.h"

/******************************************************************************/
/********************** Macros and Constants Definitions **********************/
/******************************************************************************/
#define MSEQ_BASEADDR 0

#define CORR_RST_REG  16
#define MPUSH_REG	  20
#define DATA_SEL_REG  24

#define CORR_SIZE_POW 7
#define CORR_SIZE	  (1 << CORR_SIZE_POW)
#define TSIGNAL_SIZE  128
#define ZERO_HOLD	  1

#define AMPLITUDE	  1023
#define PERIOD		  8
#define N 			  500

#define adc_corr_read(regAddr) 			Xil_In32(ADC_CORR_PACK_BASEADDR + (uint32_t)(regAddr))
#define adc_corr_write(regAddr, data)	Xil_Out32(ADC_CORR_PACK_BASEADDR + (uint32_t)regAddr, (uint32_t)data)

enum adc_data_select
{
	DATA_SEL_CHAN,
	DATA_SEL_DOUBLEDATA,
	DATA_SEL_CORR
};

void adc_dac_init(struct ad9361_rf_phy *phy);
void sync_data_start(struct ad9361_rf_phy *phy, uint8_t no, uint32_t start_address, uint32_t length);
void adc_datasel(enum adc_data_select sel);
void corr_reset(void);
void chan_transieve (struct ad9361_rf_phy *phy, uint8_t no);
void corr_transieve (struct ad9361_rf_phy *phy, uint8_t no);
#endif /* ADC_CORR_PACK_H_ */
