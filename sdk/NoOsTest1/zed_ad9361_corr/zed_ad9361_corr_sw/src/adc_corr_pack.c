/*
 * adc_corr_pack.c
 *
 *  Created on: 25.02.2015
 *      Author: ����
 */
/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "adc_corr_pack.h"
#include "parameters.h"
#include "util.h"
#include <stdint.h>
#include <xil_io.h>
#include <xil_cache.h>

/******************************************************************************/
/************************ Variables Definitions *******************************/
/******************************************************************************/
uint32_t bit_mseq1[CORR_SIZE/32], bit_mseq2[CORR_SIZE/32];
extern int16_t  *adc_buff_i1, *adc_buff_q1;
extern int32_t  *corr_buff_i, *corr_buff_q;
extern uint32_t adc_buff_size;
/******************************************************************************/
/********************** Macros and Constants Definitions **********************/
/******************************************************************************/

void adc_datasel(enum adc_data_select sel)
{
	switch (sel) {
	case DATA_SEL_CHAN:
		adc_corr_write(DATA_SEL_REG, 0);
		break;
	case DATA_SEL_CORR:
		adc_corr_write(DATA_SEL_REG, 1);
		break;
	case DATA_SEL_DOUBLEDATA:
		adc_corr_write(DATA_SEL_REG, 2);
		break;
	}
}

void corr_reset(void)
{
	uint32_t reg_val;

	adc_corr_write(CORR_RST_REG, 0x00000001);
	do {
		reg_val = adc_corr_read(CORR_RST_REG);
	}
	while(reg_val == 1);
}

void mput(const uint32_t *bit_mseq) {
	int i;
	uint32_t reg_val;
	for (i=0;i<CORR_SIZE/32;i++) {
		adc_corr_write(MSEQ_BASEADDR + i*4, bit_mseq[i]);
	}
	adc_corr_write(MPUSH_REG, 1);
	do {
			reg_val = adc_corr_read(MPUSH_REG);
	}
	while(reg_val == 1);
}

void adc_dac_init(struct ad9361_rf_phy *phy)
{
	int32_t mseq1[CORR_SIZE], mseq2[CORR_SIZE];
	int32_t datai, dataq;
	int i,j, pow;

	mseq1[0] = 1; mseq1[1] = 1;
	mseq2[0] = 1; mseq2[1] = -1;
	for (i=1;i<CORR_SIZE_POW;i++) {
		pow = 1<<i;
		for (j=0;j<pow;j++) {
			mseq1[pow + j] = mseq2[j];
			mseq2[pow + j] = -mseq2[j];
		}
		for (j=0;j<pow;j++) {
			mseq2[j] = mseq1[j];
		}
	}

	for (i=0;i<(CORR_SIZE/32);i++) {
		bit_mseq1[i] = 0;
		bit_mseq2[i] = 0;
		for (j=0;j<32;j++) {
			if (mseq1[32*i + j]==-1) {
				bit_mseq1[i] = bit_mseq1[i] | (1 << j);
			}
			if (mseq2[32*i + j]==-1) {
				bit_mseq2[i] = bit_mseq2[i] | (1 << j);
			}
		}
	}

	if(phy->pdata->rx2tx2) {

		for (i = 0; i < (ZERO_HOLD * 2); i+=2) {
			Xil_Out32(DAC_DDR_BASEADDR + i * 4, 0);
			Xil_Out32(DAC_DDR_BASEADDR + (i+1) * 4, 0);
		}
		for(i = 0; i < (CORR_SIZE * 2); i += 2) {
			datai = ((AMPLITUDE*mseq1[i / 2]) << 20) & 0xFFFF0000;
			dataq = ((AMPLITUDE*mseq1[i / 2]) << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + 8*ZERO_HOLD + i * 4, datai | dataq);
			Xil_Out32(DAC_DDR_BASEADDR + 8*ZERO_HOLD + (i+1) * 4, 0);
		}
		for (i = 0; i < (ZERO_HOLD * 2); i+=2) {
			Xil_Out32(DAC_DDR_BASEADDR + 8*ZERO_HOLD + CORR_SIZE * 8 + i * 4, 0);
			Xil_Out32(DAC_DDR_BASEADDR + 8*ZERO_HOLD + CORR_SIZE * 8 + (i+1) * 4, 0);
		}


		for (i = 0; i < (ZERO_HOLD * 2); i+=2) {
			Xil_Out32(DAC_DDR_BASEADDR + 16*ZERO_HOLD + CORR_SIZE * 8, 0);
			Xil_Out32(DAC_DDR_BASEADDR + 16*ZERO_HOLD + CORR_SIZE * 8 + 4, 0);
		}
		for(i = 0; i < (CORR_SIZE * 2); i += 2) {
			datai = ((AMPLITUDE*mseq2[i / 2]) << 20) & 0xFFFF0000;
			dataq = ((AMPLITUDE*mseq2[i / 2]) << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + 24*ZERO_HOLD + CORR_SIZE * 8 + i * 4, datai | dataq);
			Xil_Out32(DAC_DDR_BASEADDR + 24*ZERO_HOLD + CORR_SIZE * 8 + (i+1) * 4, 0);
		}
		for (i = 0; i < (ZERO_HOLD * 2); i+=2) {
			Xil_Out32(DAC_DDR_BASEADDR + 24*ZERO_HOLD + CORR_SIZE * 16, 0);
			Xil_Out32(DAC_DDR_BASEADDR + 24*ZERO_HOLD + CORR_SIZE * 16 + 4, 0);
		}


		for (i = 0; i < (ZERO_HOLD * 2); i+=2) {
			Xil_Out32(DAC_DDR_BASEADDR + 32*ZERO_HOLD + CORR_SIZE * 16, 0);
			Xil_Out32(DAC_DDR_BASEADDR + 32*ZERO_HOLD + CORR_SIZE * 16 + 4, 0);
		}
		for(i = 0; i < (TSIGNAL_SIZE * 2); i += 2) {
			datai = ((i/2)%PERIOD)>(PERIOD/2-1) ? AMPLITUDE : -AMPLITUDE;
			datai = (datai << 20) & 0xFFFF0000;
			dataq = (((i/2)+PERIOD/4)%PERIOD)>(PERIOD/2-1) ? AMPLITUDE : -AMPLITUDE;
			dataq = (dataq << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + 40*ZERO_HOLD + CORR_SIZE * 16 + i * 4, datai | dataq);
			Xil_Out32(DAC_DDR_BASEADDR + 40*ZERO_HOLD + CORR_SIZE * 16 + (i+1) * 4, 0);
		}
		Xil_Out32(DAC_DDR_BASEADDR + 40*ZERO_HOLD + CORR_SIZE * 16 + TSIGNAL_SIZE * 8, 0);
		Xil_Out32(DAC_DDR_BASEADDR + 40*ZERO_HOLD + CORR_SIZE * 16 + TSIGNAL_SIZE * 8 + 4, 0);
	}
	else {
		for(i = 0; i < CORR_SIZE; i += 1) {
			datai = ((AMPLITUDE*mseq1[i / 2]) << 20) & 0xFFFF0000;
			dataq = ((AMPLITUDE*mseq1[i / 2]) << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + i * 4, datai | dataq);
		}
		for(i = 0; i < CORR_SIZE; i += 1) {
			datai = ((AMPLITUDE*mseq2[i / 2]) << 20) & 0xFFFF0000;
			dataq = ((AMPLITUDE*mseq2[i / 2]) << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + CORR_SIZE * 4 + i * 4, datai | dataq);
		}
		for(i = 0; i < TSIGNAL_SIZE; i += 1) {
			datai = (i<20 || i>(TSIGNAL_SIZE-20)) ? 0 : \
					(i%PERIOD)>(PERIOD/2-1) ? AMPLITUDE : -AMPLITUDE;
			datai = (datai << 20) & 0xFFFF0000;
			dataq = (i<20 || i>(TSIGNAL_SIZE-20)) ? 0 : \
					((i+PERIOD/4)%PERIOD)>(PERIOD/2-1) ? AMPLITUDE : -AMPLITUDE;
			dataq = (dataq << 4) & 0xFFFF;
			Xil_Out32(DAC_DDR_BASEADDR + CORR_SIZE * 8 + i * 4, datai | dataq);
		}
	}
	Xil_DCacheFlush();

	nd_dac_init(phy, (CORR_SIZE*8 + ZERO_HOLD*16));
	adc_init(phy);
	adc_corr_write(DATA_SEL_REG, 1);
}

void sync_data_start(struct ad9361_rf_phy *phy, uint8_t no, uint32_t start_address, uint32_t length)
{
	uint32_t reg_val;
	uint32_t transfer_id;

	switch(no) {
	case 1:
		mput(bit_mseq1);
		dac_dma_write(AXI_DMAC_REG_SRC_ADDRESS, DAC_DDR_BASEADDR);
		if (phy->pdata->rx2tx2) dac_dma_write(AXI_DMAC_REG_X_LENGTH, (CORR_SIZE * 8 + ZERO_HOLD * 16) - 1);
		else dac_dma_write(AXI_DMAC_REG_X_LENGTH, CORR_SIZE * 4 - 1);
		break;
	case 2:
		mput(bit_mseq2);
		if (phy->pdata->rx2tx2) {
			dac_dma_write(AXI_DMAC_REG_SRC_ADDRESS, DAC_DDR_BASEADDR + (CORR_SIZE * 8 + ZERO_HOLD * 16));
			dac_dma_write(AXI_DMAC_REG_X_LENGTH, (CORR_SIZE * 8 + ZERO_HOLD * 16) - 1);
		} else {
			dac_dma_write(AXI_DMAC_REG_SRC_ADDRESS, DAC_DDR_BASEADDR + CORR_SIZE * 4);
			dac_dma_write(AXI_DMAC_REG_X_LENGTH, CORR_SIZE * 4 - 1);
		}
		break;
	case 3:
		if (phy->pdata->rx2tx2) {
			dac_dma_write(AXI_DMAC_REG_SRC_ADDRESS, DAC_DDR_BASEADDR + (CORR_SIZE * 16 + ZERO_HOLD * 32));
			dac_dma_write(AXI_DMAC_REG_X_LENGTH, (TSIGNAL_SIZE * 8 + ZERO_HOLD * 16) - 1);
		} else {
			dac_dma_write(AXI_DMAC_REG_SRC_ADDRESS, DAC_DDR_BASEADDR + CORR_SIZE * 8);
			dac_dma_write(AXI_DMAC_REG_X_LENGTH, TSIGNAL_SIZE * 4 - 1);
		}
		break;
	default:
		break;
	}

	adc_dma_write(AXI_DMAC_REG_CTRL, 0x0);
	adc_dma_write(AXI_DMAC_REG_CTRL, AXI_DMAC_CTRL_ENABLE);

	adc_dma_write(AXI_DMAC_REG_IRQ_MASK, 0x0);

	adc_dma_read(AXI_DMAC_REG_TRANSFER_ID, &transfer_id);
	adc_dma_read(AXI_DMAC_REG_IRQ_PENDING, &reg_val);
	adc_dma_write(AXI_DMAC_REG_IRQ_PENDING, reg_val);

	adc_dma_write(AXI_DMAC_REG_DEST_ADDRESS, start_address);
	adc_dma_write(AXI_DMAC_REG_DEST_STRIDE, 0x0);
	adc_dma_write(AXI_DMAC_REG_X_LENGTH, length - 1);
	adc_dma_write(AXI_DMAC_REG_Y_LENGTH, 0x0);

	corr_reset();
	adc_dma_write(AXI_DMAC_REG_START_TRANSFER, 0x1);
	dac_dma_write(AXI_DMAC_REG_START_TRANSFER, 0x1);

	/* Wait until the new transfer is queued. */
	do {
		adc_dma_read(AXI_DMAC_REG_START_TRANSFER, &reg_val);
	}
	while(reg_val == 1);

	/* Wait until the current transfer is completed. */
	do {
		adc_dma_read(AXI_DMAC_REG_IRQ_PENDING, &reg_val);
	}
	while(reg_val != (AXI_DMAC_IRQ_SOT | AXI_DMAC_IRQ_EOT));
	adc_dma_write(AXI_DMAC_REG_IRQ_PENDING, reg_val);

	/* Wait until the transfer with the ID transfer_id is completed. */
	do {
		adc_dma_read(AXI_DMAC_REG_TRANSFER_DONE, &reg_val);
	}
	while((reg_val & (1 << transfer_id)) != (1 << transfer_id));

	Xil_DCacheInvalidateRange(start_address, N*8);
}

void corr_transieve (struct ad9361_rf_phy *phy, uint8_t no) {
	uint32_t index;
	uint32_t data;

	adc_datasel(DATA_SEL_CORR);
	sync_data_start(phy, no, ADC_DDR_BASEADDR, 8*N);
	Xil_DCacheInvalidateRange(ADC_DDR_BASEADDR, 8*adc_buff_size);

	for(index=0; index<2*adc_buff_size; index+=2) {
		data = Xil_In32((ADC_DDR_BASEADDR + (index * 4)));
		corr_buff_i[index/2] = (int32_t)data;
		data = Xil_In32((ADC_DDR_BASEADDR + ((index+1) * 4)));
		corr_buff_q[index/2] = (int32_t)data;
	}

	printf("\nI1 channel:\n");
	for(index=0; index<adc_buff_size; index++) {
		printf("%d ", (int)corr_buff_i[index]);
	}
	printf("\nQ1 channel:\n");
	for(index=0; index<adc_buff_size; index++) {
		printf("%d ", (int)corr_buff_q[index]);
	}
}

void chan_transieve (struct ad9361_rf_phy *phy, uint8_t no) {
	uint32_t index;
	uint32_t data;

	adc_datasel(DATA_SEL_CHAN);
	sync_data_start(phy, no, ADC_DDR_BASEADDR, 8*N);
	Xil_DCacheInvalidateRange(ADC_DDR_BASEADDR, 8*adc_buff_size);

	for(index=0; index<2*adc_buff_size; index+=1) {
		data = Xil_In32((ADC_DDR_BASEADDR + (index * 4)));
		adc_buff_i1[index/2] = (uint16_t)(data & 0xFFFF);
		adc_buff_q1[index/2] = (uint16_t)((data >> 16) & 0xFFFF);
	}

	printf("\nI1 channel:\n");
	for(index=0; index<adc_buff_size; index++) {
		printf("%d ", (int)adc_buff_i1[index]);
	}
	printf("\nQ1 channel:\n");
	for(index=0; index<adc_buff_size; index++) {
		printf("%d ", (int)adc_buff_q1[index]);
	}
}
