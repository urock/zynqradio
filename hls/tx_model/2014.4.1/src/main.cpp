// ConsoleApplication16.cpp : Defines the entry point for the console application.


#include "ofdm.h"
#include "fpga.h"

struct config2 : hls::ip_fft::params_t {
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
//    static const unsigned config_width = 16;
    static const unsigned config_width = 8;			// for 2014.4

//    static const unsigned scaling_opt = hls::ip_fft::unscaled;
    static const unsigned scaling_opt = hls::ip_fft::block_floating_point;

    static const unsigned phase_factor_width = 24;

};

typedef hls::ip_fft::config_t<config2> config_cpu;
typedef hls::ip_fft::status_t<config2> status_cpu;



typedef ap_int<4> ap_int4;


int equal(float A, float B);

void QAM_modulator_cpu(short Signal[SymbolLengthHwords], complex<ap_int4> res[SymbolLengthPoints]);


unsigned int grayencode(unsigned int g);
void process_symbol_cpu(int i, int j, volatile short *bus_in, volatile float *bus_out);



int cpu_top_words(
//	unsigned int InputLength, 		// in dwords
	volatile short *bus_in,
	volatile float *bus_out
	)
{
	#pragma HLS INTERFACE ap_bus port=bus_in
	#pragma HLS INTERFACE ap_bus port=bus_out


//	f_loop: for(int i=0; i<frame_number; i++){
//
//		s_loop: for(int j=0; j<Symb_in_frame; j++) {

//			process_symbol(i, j, bus_in,bus_out);
			process_symbol_cpu(0, 0, bus_in,bus_out);

//		}
//	}

	return 0;
}


// i - frame number
// j - symbol number
void process_symbol_cpu(int i, int j, volatile short *bus_in, volatile float *bus_out) {


	int with = 0;

	short SymbolSignal[SymbolLengthHwords];

	complex<ap_int4>	SymbolPoints[SymbolLengthPoints];


    complex<float> Ifft[Nfft];
    complex<float> Ifftout[Nfft];

	complex<float> ofdm[Nfft + 2*Nprefix];

	float	temp_bus_out[Nfft + 2*Nprefix];


	config_cpu fft_config;
    status_cpu fft_status;

	fft_config.setDir(0);	// backward or inverse
	//fft_config.setDir(1);	// forward
	//fft_config.setSch(0x2AB);


	data_out_t temp_real_val, temp_imag_val;
	int t;

	memcpy1: memcpy(SymbolSignal,(const short*)(bus_in + j*SymbolLengthHwords + i*Symb_in_frame*SymbolLengthHwords ),(SymbolLengthHwords)*sizeof(short));




	QAM_modulator_cpu(SymbolSignal, SymbolPoints);


#ifdef DEBUG

	printf("Symbol Signal: \n");
	for(int i=0; i<SymbolLengthHwords; i++) {

		printf("i=%d: 0x%x ",i,SymbolSignal[i]);
	}
	printf("\n\n");


	printf("CPU QAM start vals: \n");
	for(int i=0; i<200; i++) {
		ap_int4 a = SymbolPoints[i].real();
		ap_int4 b = SymbolPoints[i].imag();

		int a1 = a;
		int b1 = b;

		printf("{%d %d}\n",a1, b1);
	}
	printf("\n\n");


#endif



	ifft_loop: for (int t = 0; t < Nfft; t++ ) {

		if (t == 0)
			Ifft[t] = 0;
		else if (t < Subcariers+1)
			Ifft[t] = SymbolPoints[t-1];
		else if (t < Nfft - Subcariers)
			Ifft[t] = 0;
		else
			Ifft[t] = SymbolPoints[t-Nfft+2*Subcariers];
	}


#ifdef DEBUG

	printf("Real part of Ifft: \n");

	for(t = 0; t < 100; t++)  {
		temp_real_val = Ifft[t].real();
//		printf("%.2f ", temp_real_val.to_double());		// for ap_fixed data type
		printf("%.2f ", temp_real_val);
	}

	printf("\n\n");

	printf("Imag part of Ifft: \n");

	for(t = 0; t < 100; t++)  {
		temp_imag_val = Ifft[t].imag();
		printf("%.2f ", temp_imag_val );
	}

	printf("\n\n");

#endif


	// FFT IP
	hls::fft<config2>(Ifft, Ifftout, &fft_status, &fft_config);



	float C = 4.0f;

	for(t=0; t < Nprefix; t++) {
		if(with)  ofdm[(Nfft+Nprefix)*j+t] = Ifftout[Nfft-Nprefix+t]*(C*windows[Nprefix-t -1]); //prefix with
		else 	  ofdm[(Nfft+2*Nprefix)*j+t] = Ifftout[Nfft-Nprefix+t]*(C*windows[Nprefix-t -1]); //prefix  without
	}

	for(t = 0; t < Nfft; t++)  {
		if(with)  ofdm[(Nfft+Nprefix)*j+Nprefix+t] = C*Ifftout[t];//main  with
		else 	  ofdm[(Nfft+2*Nprefix)*j+Nprefix+t] = C*Ifftout[t];// without
	}

	for(t = 0; t < Nprefix; t++) {
		if(with)  ofdm[(Nfft+Nprefix)*j+Nprefix+Nfft+t] = C*Ifftout[t]*windows[t];//sufix //with
		else  	  ofdm[(Nfft+2*Nprefix)*j+Nprefix+Nfft+t] = C*Ifftout[t]*windows[t];//sufix //without
	}

#ifdef DEBUG

	printf("Real part of Ifftout: \n");

	for(t = 0; t < 100; t++)  {
		temp_real_val = Ifftout[t].real();
		printf("%.2f ", temp_real_val);
	}

	printf("\n\n");

	printf("Imag part of Ifftout: \n");

	for(t = 0; t < 100; t++)  {
		temp_imag_val = Ifftout[t].imag();
		printf("%.2f ", temp_imag_val );
	}

	printf("\n\n");

	printf("Real part of OFDM: \n");

	for(t = 0; t < Nfft+2*Nprefix; t++)  {
		temp_real_val = ofdm[t].real();
		printf("%.2f ", temp_real_val);
	}

	printf("\n\n");

	printf("Imag part of OFDM: \n");

	for(t = 0; t < Nfft+2*Nprefix; t++)  {
		temp_imag_val = ofdm[t].imag();
		printf("%.2f ", temp_imag_val );
	}

	printf("\n\n");


	return;


#endif


	memcpy((float *)(bus_out + 2*(SymbLengthOut)*(i*Symb_in_frame + j)), (float*)ofdm, 2*SymbLengthOut*sizeof(float));



}


// ��� ��������. � ����� ���������� QAM = 64 �� ����� ���������� ���� 6 ���. ���������� ���� ����������� �� 16*3 = 48 ���
void QAM_modulator_cpu(short Signal[SymbolLengthHwords], complex<ap_int4> res[SymbolLengthPoints])
{

	bit b0, b1, b2, b3, b4, b5;

	int res_index = 0;

	// scan by 3 dwords
	q1: for (int word_cnt=0; word_cnt<=SymbolLengthHwords; word_cnt+=3) {


		// scan through word by blocks of qam_k = 6 bits. 16*3 = 48
		q2: for (int i=0; i<8; i++ ) {

			if (i < 2) {

				b0 = (bit)(Signal[word_cnt] >>  i*qam_k     );
				b1 = (bit)(Signal[word_cnt] >> (i*qam_k + 1));
				b2 = (bit)(Signal[word_cnt] >> (i*qam_k + 2));
				b3 = (bit)(Signal[word_cnt] >> (i*qam_k + 3));
				b4 = (bit)(Signal[word_cnt] >> (i*qam_k + 4));
				b5 = (bit)(Signal[word_cnt] >> (i*qam_k + 5));

			} else if (i == 2) {

				b0 = (bit)(Signal[word_cnt] >> 12);
				b1 = (bit)(Signal[word_cnt] >> 13);
				b2 = (bit)(Signal[word_cnt] >> 14);
				b3 = (bit)(Signal[word_cnt] >> 15);
				b4 = (bit)(Signal[word_cnt+1] >> 0);
				b5 = (bit)(Signal[word_cnt+1] >> 1);

			} else if (i < 5) {

				b0 = (bit)(Signal[word_cnt+1] >>  (i*qam_k-16)     );
				b1 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 1));
				b2 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 2));
				b3 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 3));
				b4 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 4));
				b5 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 5));


			} else if (i == 5) {

				b0 = (bit)(Signal[word_cnt+1] >> 14);
				b1 = (bit)(Signal[word_cnt+1] >> 15);
				b2 = (bit)(Signal[word_cnt+2] >> 0);
				b3 = (bit)(Signal[word_cnt+2] >> 1);
				b4 = (bit)(Signal[word_cnt+2] >> 2);
				b5 = (bit)(Signal[word_cnt+2] >> 3);


			} else {

				b0 = (bit)(Signal[word_cnt+2] >>  (i*qam_k-32)     );
				b1 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 1));
				b2 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 2));
				b3 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 3));
				b4 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 4));
				b5 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 5));
			}


			res[res_index].real(alpha[grayencode((unsigned int)(4*b0+2*b1+b2))]);
			res[res_index].imag(alpha[grayencode((unsigned int)(4*b3+2*b4+b5))]);
			res_index++;

		}


	}

}



unsigned int grayencode(unsigned int g) { // grayencode function
    return g ^ (g >> 1);
}


#include <time.h>





int main()
{
	int error=0;

	int *InputWords = (int *)malloc(Bit_Quantity/32*sizeof(int)); // input array of 32bits words

	int *InputBits = (int *)malloc(Bit_Quantity*sizeof(int)); // input array of bits

	float *ofdm_cpu = (float *)malloc(Symb_in_frame*(Nfft + 2*Nprefix)*2*sizeof(float));


	my_complex_int *cpu_results = (my_complex_int *)malloc(Symb_in_frame*(Nfft + 2*Nprefix)*sizeof(my_complex_int));
	my_complex_int *fpga_results = (my_complex_int *)malloc(Symb_in_frame*(Nfft + 2*Nprefix)*sizeof(my_complex_int));

	hls::stream<ad_str_t>      str_rl;
	hls::stream<ad_str_t>      str_im;

	ad_str_t data_out;


//	window_type window1[Nprefix];
//
//	for (int j=0;j<Nprefix;j++){
//			//window1[j] = 0.5 * ( 1 + cosf(PI/(Nprefix+1) * j) );
//
//
//			window1[j] = (window_type)(1024 * 0.5 * ( 1 + cosf(PI/(Nprefix+1) * j) ));
//			printf("%d, ",(int)window1[j]);
//		}
//
//
//	return 0;


	printf("Point_Quantity = %d\n",Point_Quantity);
	printf("Bit_Quantity = %d\n\n",Bit_Quantity);

	srand (time(NULL));

	
	
	// generate input array
	printf("\n\nInput Bytes: ");
	for ( int jj = 0; jj < (Bit_Quantity/32)/2 + 1; jj++ ) {
		//InputWords[jj] = rand() + (rand() << 16);
		InputWords[jj] = (jj << 24) + ((jj+3) << 16) + ((jj+13) << 8) + jj;

		printf("i=%d: 0x%x ", jj, InputWords[jj]);

		for (int l=0; l<32; l++)
			InputBits[jj*32 + l] = (InputWords[jj] >> l) & 0x000000001;
	}
	printf("\n\n");





	// CPU tx engine function call. It generates float output
	cpu_top_words((short*)InputWords,ofdm_cpu);

	printf("CPU OK\n");

	// hardware function call. fixed point output
	tx_engine((short*)InputWords,str_rl, str_im);

	
	// now we neeed to compare ofdm_cpu cpu results and hardware streams str_rl and str_im
	for(int t = 0; t < Nfft+2*Nprefix; t++) {


		// convert to int for compare
		cpu_results[t].real = lrintf(ofdm_cpu[2*t]);
		cpu_results[t].imag = lrintf(ofdm_cpu[2*t + 1]);

		str_rl.read(data_out);
		fpga_results[t].real = (int)data_out;
		str_im.read(data_out);
		fpga_results[t].imag = (int)data_out;
	}



	//compare results
	int error_real = 0;
	int error_imag = 0;

	int cnt = 0;

	for(int t = 0; t < Nfft+2*Nprefix; t++)  {





		if (cpu_results[t].real != fpga_results[t].real) {
			error_real++;
			if (cnt++ < 10)
				printf("Error at t = %d, cpu = %.3f, \tINT: cpu = %d, fpga = %d\n",t,ofdm_cpu[2*t], cpu_results[t].real, fpga_results[t].real );
		}

		if (cpu_results[t].imag != fpga_results[t].imag)
			error_imag++;

	}


	printf("Real part of OFDM CPU: \n");

	for(int t = 0; t < Nfft+2*Nprefix; t++)  {
		printf("%.2f ", ofdm_cpu[2*t]);
	}

	printf("\n\n");

	printf("Real part of OFDM CPU int: \n");

	int max = 0;

	for(int t = 0; t < Nfft+2*Nprefix; t++)  {
		printf("%d ", cpu_results[t].real);
		if (abs(cpu_results[t].real) > max)
			max = abs(cpu_results[t].real);
	}


	printf("\nmax = %d\n",max);




	max = 0;

	printf("Real part of OFDM FPGA int: \n");

	for(int t = 0; t < Nfft+2*Nprefix; t++)  {
		printf("%d ", fpga_results[t].real);
		if (abs(fpga_results[t].real) > max)
			max = abs(fpga_results[t].real);
	}

	printf("\nmax = %d\n",max);





	if (!(error_real) &&  !(error_imag))
		printf("\n\nTEST OK!\n\n");
	else
		printf("\n\nTest Error!!! \nerror_real = %d, \nerror_imag = %d\n\n", error_real, error_imag);




	free(InputBits);
	return 0;
}

#define absTol          0.1f
#define relTol          0.1f



// function to compare float numbers - not used with fixed point implemenation
int equal(float A, float B)
{
         return (abs(A - B) <= max(absTol, relTol * max(abs(A), abs(B))));

}




