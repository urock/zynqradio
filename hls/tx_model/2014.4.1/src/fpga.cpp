// ConsoleApplication16.cpp : Defines the entry point for the console application.

#include "ofdm.h"
#include "fpga.h"

unsigned int grayencode_fpga(unsigned int g) { // grayencode function
    return g ^ (g >> 1);
}


void apply_windows_function(	complex<data_out_t>  Ifftout[Nfft],
								ad_str_t	real_channel[2*Nprefix + Nfft],
								ad_str_t	imag_channel[2*Nprefix + Nfft]
								)
{

	window_t ifft_out_r, ifft_out_i;
	window_t rl, im;

	// apply windows function
	windows_loop: for (int t=0; t<2*Nprefix + Nfft; t++) {

#pragma HLS PIPELINE

		if (t < Nprefix) {


			ifft_out_r = Ifftout[Nfft-Nprefix+t].real();
			ifft_out_i = Ifftout[Nfft-Nprefix+t].imag();

			rl = ifft_out_r*window_fixed[Nprefix-t -1];
			im = ifft_out_i*window_fixed[Nprefix-t -1];


//			printf("fpga %d: Ifftout: %.6f \t conv: %.6f window: %.6f \t *window: %.6f \n", // compare:%.3f \t rl: %d\n",
//					t,
//					Ifftout[Nfft-Nprefix+t].real().to_float(),
//					ifft_out_r.to_float(),
//					window_fixed[Nprefix-t-1].to_float(),
//					rl.to_float());



		} else if (t <(Nprefix + Nfft)) {

			rl = Ifftout[t - Nprefix].real();
			im = Ifftout[t - Nprefix].imag();

		} else {

			ifft_out_r = Ifftout[t - Nprefix - Nfft].real();
			ifft_out_i = Ifftout[t - Nprefix - Nfft].imag();

			rl = ifft_out_r*window_fixed[t - Nprefix - Nfft];
			im = ifft_out_i*window_fixed[t - Nprefix - Nfft];


		}

		int C = 5;


		real_channel[t] = (rl << C);
		imag_channel[t] = (im << C);
	}


}


void create_ifft_array(	complex<fft_in_t>	SymbolPoints[SymbolLengthPoints],
						complex<fft_in_t>  	Ifft[Nfft]
						)
{

	complex<fft_in_t> temp_reg;

	// create fft input array
	ifft_loop: for (int t = 0; t < Nfft; t++ ) {
#pragma HLS PIPELINE

		if (t == 0) {

			temp_reg.real(0.0);
			temp_reg.imag(0.0);

		} else if (t < Subcariers+1) {

			temp_reg = SymbolPoints[t-1];

		} else if (t < Nfft - Subcariers)	{

			temp_reg.real(0.0);
			temp_reg.imag(0.0);

		} else {
			temp_reg = SymbolPoints[t-Nfft+2*Subcariers];
		}

		Ifft[t] = temp_reg;
	}
}


// ��� ��������. � ����� ���������� QAM = 64 �� ����� ���������� ���� 6 ���. ���������� ���� ����������� �� 16*3 = 48 ���
void QAM_modulator_fpga_words(	short Signal[SymbolLengthHwords],
								complex<fft_in_t> res[SymbolLengthPoints]
								)
{
//#pragma HLS dataflow
	bit b0, b1, b2, b3, b4, b5;

	int res_index = 0;

	complex<fft_in_t> temp_reg;

	// scan by 3 dwords
	q1: for (int word_cnt=0; word_cnt<=SymbolLengthHwords; word_cnt+=3) {


		// scan through word by blocks of qam_k = 6 bits. 16*3 = 48
		q2: for (int i=0; i<8; i++ ) {
#pragma HLS PIPELINE

			if (i < 2) {

				b0 = (bit)(Signal[word_cnt] >>  i*qam_k     );
				b1 = (bit)(Signal[word_cnt] >> (i*qam_k + 1));
				b2 = (bit)(Signal[word_cnt] >> (i*qam_k + 2));
				b3 = (bit)(Signal[word_cnt] >> (i*qam_k + 3));
				b4 = (bit)(Signal[word_cnt] >> (i*qam_k + 4));
				b5 = (bit)(Signal[word_cnt] >> (i*qam_k + 5));

			} else if (i == 2) {

				b0 = (bit)(Signal[word_cnt] >> 12);
				b1 = (bit)(Signal[word_cnt] >> 13);
				b2 = (bit)(Signal[word_cnt] >> 14);
				b3 = (bit)(Signal[word_cnt] >> 15);
				b4 = (bit)(Signal[word_cnt+1] >> 0);
				b5 = (bit)(Signal[word_cnt+1] >> 1);

			} else if (i < 5) {

				b0 = (bit)(Signal[word_cnt+1] >>  (i*qam_k-16)     );
				b1 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 1));
				b2 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 2));
				b3 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 3));
				b4 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 4));
				b5 = (bit)(Signal[word_cnt+1] >> ((i*qam_k-16) + 5));


			} else if (i == 5) {

				b0 = (bit)(Signal[word_cnt+1] >> 14);
				b1 = (bit)(Signal[word_cnt+1] >> 15);
				b2 = (bit)(Signal[word_cnt+2] >> 0);
				b3 = (bit)(Signal[word_cnt+2] >> 1);
				b4 = (bit)(Signal[word_cnt+2] >> 2);
				b5 = (bit)(Signal[word_cnt+2] >> 3);


			} else {

				b0 = (bit)(Signal[word_cnt+2] >>  (i*qam_k-32)     );
				b1 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 1));
				b2 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 2));
				b3 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 3));
				b4 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 4));
				b5 = (bit)(Signal[word_cnt+2] >> ((i*qam_k-32) + 5));
			}


			temp_reg.real(alpha_f[grayencode_fpga((unsigned int)(4*b0+2*b1+b2))]);
			temp_reg.imag(alpha_f[grayencode_fpga((unsigned int)(4*b3+2*b4+b5))]);

			res[res_index] = temp_reg;

			res_index++;

		}


	}

}



void fft_top(short SymbolSignal[SymbolLengthHwords], ad_str_t	real_channel[2*Nprefix + Nfft], ad_str_t	imag_channel[2*Nprefix + Nfft]) {


	#pragma HLS dataflow
	#pragma HLS data_pack variable=real_channel
	#pragma HLS data_pack variable=imag_channel

	complex<fft_in_t>	SymbolPoints[SymbolLengthPoints];
	complex<fft_in_t>  	Ifft[Nfft];

	complex<data_out_t>  Ifftout[Nfft];



    config_t fft_config;
    status_t fft_status;
	#pragma HLS data_pack variable=fft_config
    fft_config.setDir(0);


    // ATYTENTION
    // config_dataflow -default_channel fifo -fifo_depth 1
    // is nessesary!

	// convert data bits to QAM points
	QAM_modulator_fpga_words(SymbolSignal, SymbolPoints);

	create_ifft_array(SymbolPoints, Ifft);

    hls::fft<config1>
    (Ifft, Ifftout, &fft_status, &fft_config);


	apply_windows_function(Ifftout, real_channel, imag_channel);



}


void process_symbol(int i, int j,
		volatile short *bus_in,
		hls::stream<ad_str_t>      &str_rl,
		hls::stream<ad_str_t>      &str_im)
{

	short SymbolSignal[SymbolLengthHwords];

	ad_str_t	real_channel[2*Nprefix + Nfft];
	ad_str_t	imag_channel[2*Nprefix + Nfft];


	// read one symbol data from AXI memory
	memcpy1: memcpy(SymbolSignal,(const short*)(bus_in + j*SymbolLengthHwords + i*Symb_in_frame*SymbolLengthHwords ),(SymbolLengthHwords)*sizeof(short));


	// fft and processing
	fft_top(SymbolSignal,real_channel, imag_channel);


	// write data to output fifos
	for (int t = 0; t<2*Nprefix + Nfft; t++) {
		str_rl.write(real_channel[t]);
		str_im.write(imag_channel[t]);
	}

}



// hardware top function
int tx_engine(
	volatile short *bus_in,

	hls::stream<ad_str_t>      &str_rl,
	hls::stream<ad_str_t>      &str_im
	)
{
#pragma HLS INTERFACE s_axilite port=return offset=0xA0000000
	#pragma HLS INTERFACE m_axi port=bus_in offset=direct


#ifdef COMPARE
	// this for compare test
	process_symbol(0, 0, bus_in,str_rl, str_im);
#endif

#ifndef COMPARE

	// this is for synthesis
	f_loop: for(int i=0; i<frame_number; i++){

		s_loop: for(int j=0; j<Symb_in_frame; j++) {

			process_symbol(i, j, bus_in,str_rl, str_im);

		}
	}
#endif


	return 0;
}

