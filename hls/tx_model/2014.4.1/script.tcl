############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2014 Xilinx Inc. All rights reserved.
############################################################
open_project -reset ofdm_tx
set_top tx_engine
add_files ./src/fpga.cpp
add_files -tb ./src/main.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
config_dataflow -default_channel fifo -fifo_depth 1

#csim_design
csynth_design
export_design -format ip_catalog -vendor "urock" -library "ofdm" -version "1.1" -display_name "ofdm_tx"

