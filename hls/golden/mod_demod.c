// ConsoleApplication16.cpp : Defines the entry point for the console application.


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>


# define PI        3.14159265358979323846  /* pi */    //sublime for linux//


//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------Function block------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------------


unsigned int grayencode(unsigned int g) { // grayencode function
    return g ^ (g >> 1);
} 

unsigned int graydecode(unsigned int gray) { // graydecode function
    unsigned int bin;
    for (bin = 0; gray; gray >>= 1) {
      bin ^= gray;
    }
    return bin;
}

int* dec_to_bin (int QAM, int decimalNumber){ // decimal to binary function
	int k = (log10(QAM)/log10(2))/2;
	int i=1,j;
	int *binaryNumber = (int*)malloc(k*sizeof(int));

    for(j=0;j<k;j++){
         binaryNumber[k-i++]= decimalNumber % 2;
         decimalNumber = decimalNumber / 2;
    }
	return binaryNumber;
}

int bin_to_dec (int QAM, int *binaryNumber){ // binary to decimal function
   
    int length = (log10(QAM)/log10(2))/2;
	int i=0,j=1;
	int decimalNumber=0,remainder;

	for(i=0;i<length;i++){
    remainder=binaryNumber[length-i-1];
    decimalNumber+=remainder*j;
    j=j*2;
    }

    return decimalNumber;
}

void print_PAM(int QAM){  // simple print function to see constellation vallues
	int t;
	int N = sqrt(QAM);
	printf("PAM constellation values:");
	for (t=0;t<N;t++)
	printf("%d ",  (int)(-(2*N/2-1)+2*t));
	printf("\n");
}

void print_sig_mod(int QAM,int* arr,int Bit_Quantity) { // print function of parameteres 
	int i,j;
	int k = log10(QAM)/log10(2);
	int Point_Quantity = Bit_Quantity/k;

	printf("bits per symbol: %d ", k);
	printf("\n");
	printf("number of symbols: %d ", Point_Quantity);
	printf("\n");
	printf("constellation size: %d ", QAM);
	printf("\n");
	print_PAM(QAM);

	printf("First 24 bits of ipBit: ");
	for(j = 0; j < 20; j++){
		 printf("%d ",  arr[j]);
	}
	printf("\n\n");
 
		
}

double *Corrector(int Point_Quantity, int QAM, double* arr) {
	int i,t;
	int G =sqrt(QAM);
	int *alphaRe = (int *)malloc(31*sizeof(int)); 

	for (i=0;i<8;i++){  
		 *(alphaRe+i) = (int)(-(2*G/2-1)+2*i); //PAM constellation vallues
	}

	for(i=0;i<Point_Quantity;i++){
	if(arr[i]<=*(alphaRe)) arr[i]=*(alphaRe); // correction
	else if(arr[i]>=*(alphaRe+G-1)) arr[i]=*(alphaRe+G-1);
	else {
		for (t=0;t<G;t++) {
			if(((*(alphaRe+t)-1)<=arr[i]) && (arr[i]<(*(alphaRe+t)+1))) 
				arr[i]=*(alphaRe+t);			
			}	
		}
	}
	free(alphaRe);
	return arr;
}

fftw_complex *QAM_modulator(int QAM, int *Signal, int Bit_Quantity) {
	int i,j,z,t = 0;  //counters
	int k = log10(QAM)/log10(2); //number of bits in one point
	int N = sqrt(QAM); //number of points on Q/I axes
	int Point_Quantity = Bit_Quantity/k; //number of symbols

	int *alphaRe = (int *)malloc(QAM*sizeof(int)); //PAM constellation vallues
	int *ipBit = (int *)malloc(Bit_Quantity*sizeof(int)); //input signal
	int * real = (int*)malloc(Point_Quantity*sizeof(int)); //real part of signal
	int * imag = (int*)malloc(Point_Quantity*sizeof(int)); // imag part of signal
	int *binaryNumber = (int*)malloc(k/2*sizeof(int)); //buffer array
	fftw_complex * modRe = (fftw_complex *)fftw_malloc(Point_Quantity* sizeof(fftw_complex));

	for (i=0;i<N;i++) {  // PAM values
		 *(alphaRe+i) = (int)(-(2*sqrt(QAM)/2-1)+2*i);
	}

	for (i=0;i<Bit_Quantity;i++) { //input signal
		ipBit[i] = Signal[i];
	}

	print_sig_mod(QAM,ipBit,Point_Quantity); //information

	for (i=0;i<Point_Quantity;i++){	
		for(z=0;z<k/2;z++){
			binaryNumber[z]=ipBit[i*k+z];
		}

		real[i] = alphaRe[grayencode(bin_to_dec(QAM,binaryNumber))];

		for(z=0;z<k/2;z++)
			binaryNumber[z]=ipBit[k/2+i*k+z];

		imag[i] = alphaRe[grayencode(bin_to_dec(QAM,binaryNumber))];
	}


	for(i=0;i<Point_Quantity;i++){
		modRe[i] = real[i] + imag[i] * I;
	}



	free(alphaRe);
	free(real);
	free(imag);
	free(ipBit);

	return modRe;
}

int *QAM_demodulator(int QAM, fftw_complex * modRe, int Point_Quantity) {
	int i,z;
	int G = sqrt(QAM);//number of points on Q/I axes
	int k = log10(QAM)/log10(2);//number of bits in one point
	int Bit_Quantity = Point_Quantity*k; //number of input bits

	int *alphaRe = (int *)malloc(31*sizeof(int)); 
	for (i=0;i<G;i++) {
		 *(alphaRe+i) = (int)(-(2*G/2-1)+2*i);
	}

	double *real = (double *)malloc(Point_Quantity*sizeof(double));
	double *imag = (double *)malloc(Point_Quantity*sizeof(double));
	int *Output = (int *)malloc(Bit_Quantity*sizeof(int)); 
	int *binaryNumber = (int*)malloc(k/2*sizeof(int)); 

	for(i=0;i<Point_Quantity;i++){
		real[i]=creal(modRe[i]);
		imag[i]=cimag(modRe[i]);
		}

	Corrector(Point_Quantity,QAM,real);
	Corrector(Point_Quantity,QAM,imag);

	for (i=0;i<Point_Quantity;i++){		
		binaryNumber = dec_to_bin(QAM,(graydecode((unsigned int)(real[i]+(2*G/2-1))/2)));
		for(z=0;z<k/2;z++)
			Output[i*k+z]=binaryNumber[z];

		binaryNumber = dec_to_bin(QAM,(graydecode((unsigned int)(imag[i]+(2*G/2-1))/2)));
		for(z=0;z<k/2;z++)
			Output[k/2+i*k+z]=binaryNumber[z];
	}


	free(alphaRe);
	free(real);
	free(imag);
	free(binaryNumber);

	return Output;

}


//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------Function block end--------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------------------

int main()
{
int with = 0;
int j,i,t=0; //counters
int QAM = 64; //modulation size
int k = log10(QAM)/log10(2);//number of bits in one point
int G = sqrt(QAM);//number of points on Q/I axes
int Bit_Quantity = 2400000; //number of input bits
int Point_Quantity = Bit_Quantity/k; //number of points
int Subcariers = 100; //number of subcariers
int Symbols_quantity = Point_Quantity/(Subcariers*2);
int Symb_in_frame = 10;
int Nfft = 1024; // fftw
double PrefixRate = 0.25; // prefix
int Nprefix = PrefixRate*Nfft;
double *window = (double *)malloc(Nprefix*sizeof(double)); 

//--------------------------------------------------------------------------
fftw_complex *Ifft = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex));
fftw_complex *Ifft2 = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex));
fftw_complex *Ifftout = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex));
fftw_complex *Ifftout2 = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex));
fftw_plan p1;
p1 = fftw_plan_dft_1d(Nfft, Ifft, Ifftout, FFTW_BACKWARD, FFTW_ESTIMATE);
fftw_plan p2;
p2 = fftw_plan_dft_1d(Nfft, Ifft2, Ifftout2, FFTW_FORWARD, FFTW_ESTIMATE);
fftw_complex *OFDM = (fftw_complex *)fftw_malloc(((Nfft+2*Nprefix)*Symb_in_frame)*sizeof(fftw_complex));  //do with without

//--------------------------------------------------------------------------

int *Input = (int *)malloc(Bit_Quantity*sizeof(int));
fftw_complex * array = (fftw_complex *)fftw_malloc(Point_Quantity* sizeof(fftw_complex));
int *Output = (int *)malloc(Bit_Quantity*sizeof(int));

for ( j = 0; j < Bit_Quantity; j++ ){
		Input[j] =  (int) ((rand() / (RAND_MAX + 1.0))>0.5 ? 0 : 1); //start array
	}


array = QAM_modulator(QAM,Input,Bit_Quantity); //get array of complex points

for(i=0;i<10;i++)
	printf("%f  ",creal(array[i]));
printf ("\n");


for (j=0;j<Nprefix;j++)
	{
		window[j] = 0.5 * ( 1 + cos(PI/(Nprefix+1) * j) ); //applyig window
	}

for(j=0; j<Symb_in_frame; j++) { // OFDM forward

	Ifft[0] = 0; 
	for( t=0; t < Subcariers; t++) {

		Ifft[t+1] = array[(Subcariers*2*j)+t];
		Ifft[Nfft-Subcariers+t] = array[(Subcariers*2*j)+Subcariers+t]; 
	}

	for(t=0; t <  Nfft-Subcariers*2-1; t++)  {
		Ifft[Subcariers+t+1] = 0;
	}

	fftw_execute(p1);

	for(t=0; t < Nprefix; t++) {
		if(with)  OFDM[(Nfft+Nprefix)*j+t] += Ifftout[Nfft-Nprefix+t]*window[Nprefix-t]; //prefix with
		else 	  OFDM[(Nfft+2*Nprefix)*j+t] = Ifftout[Nfft-Nprefix+t]*window[Nprefix-t]; //prefix  without
	}

	for(t = 0; t < Nfft; t++)  {
		if(with)  OFDM[(Nfft+Nprefix)*j+Nprefix+t] = Ifftout[t];//main  with
		else 	  OFDM[(Nfft+2*Nprefix)*j+Nprefix+t] = Ifftout[t];// without
	}
 
	for(t = 0; t < Nprefix; t++) {
		if(with)  OFDM[(Nfft+Nprefix)*j+Nprefix+Nfft+t] = (Ifftout[t])*window[t];//sufix //with
		else  	  OFDM[(Nfft+2*Nprefix)*j+Nprefix+Nfft+t] = (Ifftout[t])*window[t];//sufix //without
	}

	
}

/*
FILE *fileOFDM;                             Print results into text file
fileOFDM = fopen("Input_OFDM.txt","w");
char buf[256];

for (t = 0; t < (Nfft+2*Nprefix)*Symb_in_frame; t++)
{
sprintf(buf, "%f %f" , creal(OFDM[t]), cimag(OFDM[t]));
fprintf(fileOFDM,"%s \n", buf);
}
fclose(fileOFDM);
*/

fftw_complex *Signal2 = (fftw_complex *)fftw_malloc((Nfft*Symb_in_frame)*sizeof(fftw_complex));


for(j=0; j<Symb_in_frame; j++) { // OFDM backward


	for(t=0;t<Nfft;t++)
	Ifft2[t]=(OFDM[(Nfft+Nprefix)*j+Nprefix+t]);

	fftw_execute(p2);

		for(t=0;t<Nfft;t++)
		Ifftout2[t] /= Nfft;

	for(t=0;t<Nfft;t++)
	Signal2[t+(Nfft)*j] = Ifftout2[t];
	
}

for (i=0;i<10;i++)
		printf("%f  ",  creal(Signal2[i]) );
	printf("\n\n");



t=0;
Output = QAM_demodulator(QAM,array,Point_Quantity); //get array of bits from array of complex points


	for (i=0;i<50;i++)
		printf("%d ",  Input[i]);
	printf("\n");

	for (i=0;i<50;i++)
		printf("%d ",  Output[i]);
	printf("\n");

	for (i=0;i<Bit_Quantity;i++){ //counting number of mistakes
		if(Output[i]!=Input[i]) 
		t++;
	}

	printf("Total number of mistakes: %d",t);
	printf("\n");









fftw_destroy_plan(p1);
fftw_destroy_plan(p2);
free(Input);
free(Output);
fftw_free(Ifft);
fftw_free(Ifftout);
fftw_free(Ifft2);
fftw_free(Ifftout2);
fftw_free(OFDM);
fftw_free(Signal2);
free(window);

return 0;
}
