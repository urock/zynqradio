// ConsoleApplication16.cpp : Defines the entry point for the console application.


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <fftw3.h>


# define PI        3.14159265358979323846  /* pi */    //sublime for linux//
# define Nfft 1024
# define Nprefix 256


double *window; 







unsigned int grayencode(unsigned int g) { // grayencode function
    return g ^ (g >> 1);
} 

unsigned int graydecode(unsigned int gray) {  // graydecode function
    unsigned int bin;
    for (bin = 0; gray; gray >>= 1) {
      bin ^= gray;
    }
    return bin;
}

int* dec_to_bin (int QAM, int decimalNumber){ // decimal to binary function
	int k = (log10(QAM)/log10(2))/2;
	int i=1,j;
	int *binaryNumber = (int*)malloc(k*sizeof(int));

    for(j=0;j<k;j++){
         binaryNumber[k-i++]= decimalNumber % 2;
         decimalNumber = decimalNumber / 2;
    }
	return binaryNumber;
}

int bin_to_dec (int QAM, int *binaryNumber){ // binary to decimal function
   
    int length = (log10(QAM)/log10(2))/2;
	int i=0,j=1;
	int decimalNumber=0,remainder;

	for(i=0;i<length;i++){
    remainder=binaryNumber[length-i-1];
    decimalNumber+=remainder*j;
    j=j*2;
    }

    return decimalNumber;
}

void print_QAM(int QAM){ // simple print function to see constellation vallues
	int t;
	int N = sqrt(QAM);
	printf("QAM constellation values:");
	for (t=0;t<N;t++)
	printf("%d ",  (int)(-(2*N/2-1)+2*t));
	printf("\n");
}

void print_sig_mod(int QAM,int* arr,int Bit_Quantity) { // print function of parameteres 
	int i,j;
	int k = log10(QAM)/log10(2);
	int Point_Quantity = Bit_Quantity/k;

	printf("bits per symbol: %d ", k);
	printf("\n");
	printf("number of symbols: %d ", Point_Quantity);
	printf("\n");
	printf("constellation size: %d ", QAM);
	printf("\n");
	print_QAM(QAM);

	printf("First 24 bits of ipBit: ");
	for(j = 0; j < 20; j++){
		 printf("%d ",  arr[j]);
	}
	printf("\n\n");
 

	printf("First 6 simbols : \n");
	for(i = 0; i < 5; i++)
	{
		for(j = 0; j < k; j++)
			printf("%d ", arr[k*i+j]);
		printf("\n");
	}
	printf("\n\n");
		
}


fftw_complex QAM_modulator(int QAM, int *Signal) {  //Modulator function for 4,16 or 64 QAM
	int i,z = 0;  //counters
	int k = log10(QAM)/log10(2); //number of bits in one point (log2(QAM))
	int N = sqrt(QAM); //number of points on Q/I axes
	fftw_complex res;
	int *alpha = (int *)malloc(QAM*sizeof(int)); //QAM constellation vallues. (ex: -3 -1 1 3 for 16QAM)


	int real ; //real part of signal
	int imag ; // imag part of signal
	int *binaryNumber = (int*)malloc(k/2*sizeof(int)); //buffer array

	for (i=0;i<N;i++) {  // QAM constellation values
		 *(alpha+i) = (int)(-(2*sqrt(QAM)/2-1)+2*i);
	}


	for(z=0;z<k/2;z++) binaryNumber[z]=Signal[z]; //real part of point stored in buffer array. (ex: [1][0][0] (64 QAM))

	real = alpha[ grayencode(bin_to_dec(QAM,binaryNumber)) ];  //modulation of real part

	for(z=0;z<k/2;z++) binaryNumber[z]=Signal[k/2 + z]; // imag part of point stored in buffer array. (ex: [0][1][0] (64 QAM))

	imag = alpha[ grayencode(bin_to_dec(QAM,binaryNumber)) ]; //modulation of imag part

	free(alpha);
	res = (real + imag * I);

	return res;
}


fftw_complex * fft(fftw_complex * Ifft) //fft function
{
	fftw_complex *Ifftout = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex)); //output array
	fftw_plan p1;
	p1 = fftw_plan_dft_1d(Nfft, Ifft, Ifftout, FFTW_BACKWARD, FFTW_ESTIMATE); //plan of fft
	fftw_execute(p1); //fft
	return Ifftout;
}


fftw_complex *apply_window (fftw_complex * Ifftout) // window function
{
	int t;
	fftw_complex *symbol = (fftw_complex *)fftw_malloc((Nfft+2*Nprefix)*sizeof(fftw_complex)); //output array
	for(t=0; t < Nprefix; t++) {
		 symbol[t] = Ifftout[Nfft-Nprefix+t]*window[Nprefix-t];//prefix	
	}

	for(t = 0; t < Nfft; t++)  {
		symbol[Nprefix+t] = Ifftout[t];//main	
	}
 
	for(t = 0; t < Nprefix; t++) {
		 symbol[Nprefix+Nfft+t] = Ifftout[t]*window[t];//sufix 
	}
	
	return symbol;
}


int main()
{
int with = 0;
int j,i,t=0; //counters
int QAM = 16; //modulation size
int k = log10(QAM)/log10(2);//number of bits in one point
int G = sqrt(QAM);//number of points on Q/I axes
int Bit_Quantity = 2400000; //number of input bits
int Point_Quantity = Bit_Quantity/k; //number of points
int Subcariers = 100; //number of subcariers
int Symbols_quantity = Point_Quantity/(Subcariers*2); 
int Symb_in_frame = 15;
int Frame_quantity = Symbols_quantity/Symb_in_frame; //max frame_number
int frame_number = 2;

//--------------------------------------------------------------------------
fftw_complex *Ifft = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex)); //fft input array

fftw_complex *Ifftout = (fftw_complex *)fftw_malloc(Nfft*sizeof(fftw_complex)); //fft output array

fftw_complex *OFDM = (fftw_complex *)fftw_malloc((Nprefix + (Nprefix+Nfft)*Symbols_quantity)*sizeof(fftw_complex));  //main signal output array

int *Input = (int *)malloc(Bit_Quantity*sizeof(int)); // input array of bits

fftw_complex * array = (fftw_complex *)fftw_malloc(Point_Quantity* sizeof(fftw_complex)); //array of constellation vallues

window = (double *)malloc(Nprefix*sizeof(double)); //window array

fftw_complex *symbol = (fftw_complex *)fftw_malloc((Nfft+2*Nprefix)*sizeof(fftw_complex)); //array of one symbol after window applying

//--------------------------------------------------------------------------

for ( j = 0; j < Bit_Quantity; j++ ){
		Input[j] =  (int) ((rand() / (RAND_MAX + 1.0))>0.5 ? 0 : 1);
	}

for (j=0;j<Nprefix;j++){
		window[j] = 0.5 * ( 1 + cos(PI/(Nprefix+1) * j) ); 
	}



for(i=0;i<Point_Quantity;i++) 
	array[i] = QAM_modulator(QAM,(Input+i*k));

for(i=0; i<frame_number; i++){

	for(j=0; j<Symb_in_frame; j++) {

		Ifft[0] = 0; 

		for( t=0; t < Subcariers; t++) {
			Ifft[t+1] = array[(Subcariers*2*j)+t+(Subcariers*2*Symb_in_frame)*i];
			Ifft[Nfft-Subcariers+t] = array[(Subcariers*2*j)+Subcariers+t + (Subcariers*2*Symb_in_frame)*i]; 
		}

		for(t=0; t <  Nfft-Subcariers*2-1; t++)  {
			Ifft[Subcariers+t+1] = 0;
		}

		Ifftout = fft(Ifft);
		symbol = apply_window(Ifftout);
		
		for(t=0; t<(Nfft+2*Nprefix); t++) {
		OFDM[(Nprefix+Nfft)*Symb_in_frame*i+(Nprefix+Nfft)*j+t] += symbol[t];
		}

	}
}

for(i=0; i < 10; i ++)
	printf("real: %f  ,  imag: %f \n" , creal(OFDM[i]), cimag(OFDM[i]));


//write data to file
FILE *fileOFDM; 
fileOFDM = fopen("Input_OFDM.txt","w");
char buf[256];
for (t = 0; t < (Nprefix + (Nprefix+Nfft)*Symb_in_frame)*frame_number; t++)
{
sprintf(buf, "%f %f" , creal(OFDM[t]), cimag(OFDM[t]));
fprintf(fileOFDM,"%s \n", buf);
}
fclose(fileOFDM);


//free memory
free(Input);
fftw_free(Ifft);
fftw_free(Ifftout);
fftw_free(OFDM);
free(window);
free(symbol);

return 0;
}
