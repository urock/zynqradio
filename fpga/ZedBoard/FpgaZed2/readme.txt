FpgaZed2
	Тоже самое, что FpgaZed1, но вместо ядра util_adc_pack используется ядро с коррелятором ip_cores/user_adc_corr_pack_2_7 
	
	Проект создается автоматически
	
	Для подцепления этого ядра в Vivado в TCL скрипт \fpga\ZedBoard\FpgaZed2\projects\scripts\adi_project.tcl в процедуру adi_project_create добавлены следующие строчки:
	
  set new_ip_dir $ad_hdl_dir/../../../ip_cores
  puts "Adding $new_ip_dir to repo path"
  lappend lib_dirs $new_ip_dir
  set_property ip_repo_paths $lib_dirs [current_fileset]
  update_ip_catalog
	

FpgaZed1
	fmcomms2 проект из репозитория AD для платы Zedboard