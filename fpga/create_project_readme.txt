﻿Открытие проекта FPGA
1. cd path_to_repo\fpga\FpgaZed1\projects\fmcomms2\zed
2. В файле create_project.bat проверить путь установке Vivado 2014.2 и запустить его
3. Дождаться завершения процесса создания проекта Vivado и либо
	% start_gui
	либо
	закрыть окно и открыть файл fmcomms2_zed.xpr кликом